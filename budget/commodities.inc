commodity USD
   note US dollars
   format USD 1,000.00
   nomarket

commodity CAD
   note Canadian dollars
   format CAD 1,000.00
   nomarket
   default
